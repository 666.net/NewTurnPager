package com.example.dllo.newturnpager;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;


/**
 * code is far away from bug with the animal protecting
 * <p/>
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！  凯哥 祝你一路顺风
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 */
public class MyApp extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext=this;
//        //第一：默认初始化 通常会在Application的onCreate方法里写
//        Bmob.initialize(this, "1f593c7c45cc220c981e72df2e4c7d4b");
//        // 使用推送服务时的初始化操作
//        BmobInstallation.getCurrentInstallation().save();
//        // 启动推送服务
//        BmobPush.startWork(this);
        //BGBanner需要进行初始化
        //需要进行初始化
        Fresco.initialize(this);
    }

    public  static Context getContext(){
        return  mContext;
    }
}
