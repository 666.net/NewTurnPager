package com.example.dllo.newturnpager;


import android.net.Uri;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.bgabanner.BGABanner;

public class MainActivity extends BaseActivity {
    private BGABanner banner;
    private List<View> mViews = new ArrayList<>();
    private static String url = "http://api.liwushuo.com/v2/banners?channel=IOS";

    @Override
    protected int setLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        banner = bindView(R.id.banner_main_cube);
    }

    @Override
    protected void initData() {
        NetTool netTool = new NetTool();
        mViews = getViews(4);
        banner.setViews(mViews);
        netTool.getNetData(url, MyBean.class, new NetTool.NetListener<MyBean>() {
            @Override
            public void onSuccess(MyBean myBean) {
                SimpleDraweeView simpleDraweeView;
                for (int i = 0; i < mViews.size(); i++) {
                    simpleDraweeView = (SimpleDraweeView) mViews.get(i);
                    simpleDraweeView.setImageURI(Uri.parse(myBean.getData().getBanners().get(i).getImage_url()));

                }
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

    }

    private List<View> getViews(int count) {
        List<View> views = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            views.add(getLayoutInflater().inflate(R.layout.view_image, null));
        }
        return views;
    }
}

