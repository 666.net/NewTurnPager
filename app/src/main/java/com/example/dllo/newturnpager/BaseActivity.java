package com.example.dllo.newturnpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;


/**
 * code is far away from bug with the animal protecting
 * <p/>
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！  凯哥 祝你一路顺风
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 */
public abstract class BaseActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(setLayout()!=0){
            setContentView(setLayout());//绑定布局
        }else {
           Log.e("BaseActivity", "Activity:"+this.getClass().getSimpleName()+" 没有绑定布局");
        }
        initView();
        initData();

    }


    protected  abstract  int setLayout();
    protected  abstract  void initView();
    protected  abstract  void initData();

    //简化findViewById
    protected <T extends View> T bindView(int id){
        return (T) findViewById(id);
    }
    //设置点击事件的方法,方法是不固定参数个数的,该方法可以不写
//    protected void setClick(View... views) {
//        for (View view : views) {
//            view.setOnClickListener(this);
//        }
//    }




}
