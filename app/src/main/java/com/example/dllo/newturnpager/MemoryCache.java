package com.example.dllo.newturnpager;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * code is far away from bug with the animal protecting
 * <p/>
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃永无BUG！  凯哥 祝你一路顺风
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 */
public class MemoryCache implements ImageLoader.ImageCache{
    //LruCache 是实现了 最近至少使用算法的子类
    //key 是url value是需要缓存的图片
   private LruCache<String,Bitmap> mLruCache;

   public MemoryCache(){
       //获取Android 为我们的应用提供的最大内存空间
       //我们只使用最大的内存空间的1/8来作为缓存
       int maxSize= (int) (Runtime.getRuntime().maxMemory()/8);
       //LruCache在初始化的时候  需要指定最大容量
               mLruCache=new LruCache<String,Bitmap>(maxSize){
                   //LruCache需要复写sixeOf方法用来指定Value所占的空间大小
                   @Override
                   protected int sizeOf(String key, Bitmap value) {
                       //除1024是为了将字节转换成kb
                       int valueSize=value.getByteCount()/1024;
                       return valueSize;
                   }
               };
   }

    @Override
    public Bitmap getBitmap(String url) {
        return mLruCache.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {

    }
}
